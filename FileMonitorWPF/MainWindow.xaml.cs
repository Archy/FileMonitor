﻿using FileMonitorWCF;
using System;
using System.ServiceModel;
using System.Windows;

namespace FileMonitorWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ChannelFactory<IFileMonitorConfigService> pipeFactory;
        IFileMonitorConfigService pipeProxy;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            bool result = false;
            string msg = "";
            try
            {
                pipeFactory = new ChannelFactory<IFileMonitorConfigService>(
                  new NetNamedPipeBinding(),
                  new EndpointAddress("net.pipe://localhost/PipeFileMonitor")
                );
                pipeProxy = pipeFactory.CreateChannel();

                FileMonitorConfig config = new FileMonitorConfig();

                config.Path = pathBox.Text;
                config.Mask = maskBox.Text;
                config.Recrusive = recursiveCheck.IsChecked ?? false;

                config.Changed = ChangedCheck.IsChecked ?? false;
                config.Created = CreatedCheck.IsChecked ?? false;
                config.Deleted = DeletedCheck.IsChecked ?? false;
                config.Disposed = DisposedCheck.IsChecked ?? false;
                config.Error = ErrorCheck
                    .IsChecked ?? false;
                config.Renamed = RenamedCheck.IsChecked ?? false;

                result = pipeProxy.ConfigureFileMonitor(config);
            }
            catch(Exception ex)
            {
                result = false;
                msg = ex.ToString();
            }
            resultBox.Text = result ? "Updated successfully" : "Update failed: "+ msg;
        }

        private void chooseButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                pathBox.Text = dialog.SelectedPath;
            }
        }
    }
}
