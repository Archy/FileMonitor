﻿using System.Configuration;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace FilesMonitor
{
    public partial class FilesMonitoringService : ServiceBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FilesMonitoringService));
        private FileMonitor monitor = new FileMonitor();
        private Task monitorTask;

        public FilesMonitoringService()
        {
            InitializeComponent();
            CanPauseAndContinue = true;
        }

        public void mockStart(string[] args)
        {
            OnStart(args);
        }

        protected override void OnStart(string[] args)
        {
            log.Info("File monitoring starts");
            monitorTask = Task.Run(() => monitor.Run((FileMonitorSection)ConfigurationManager.GetSection("fileMonitor")));
        }

        public void mockStop()
        {
            OnStop();
        }

        protected override void OnStop()
        {
            log.Info("File monitoring stops");
            monitor.IsRunning = false;
            monitorTask.Wait();
        }

        public void mockPause()
        {
            OnPause();
        }

        protected override void OnPause()
        {
            log.Info("File monitoring pauses");
            monitor.onPause();
        }

        public void mockContinue()
        {
            OnContinue();
        }

        protected override void OnContinue()
        {
            log.Info("File monitoring resumes");
            monitor.onContinue();
        }
    }
}
