﻿using System.ServiceProcess;
using System.Threading;

namespace FilesMonitor
{
    static class Program
    {
        const bool RUN_STANDALONE = false;

        static Program()
        {
            log4net.LogManager.GetRepository().LevelMap.Add(LogExt.RenameLevel);
            log = log4net.LogManager.GetLogger(typeof(Program));
        }
        private static readonly log4net.ILog log;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            log.Error("test");
            if(RUN_STANDALONE)
            {
                var service = new FilesMonitoringService();

                service.mockStart(args);
                //Thread.Sleep(10000);

                //service.mockPause();
                //Thread.Sleep(60000);

                //service.mockContinue();
                Thread.Sleep(120000);

                //while (true) { }

                service.mockStop();
            }
            else
            {
                /**
                 * Developer Command Prompt for VS2015 as Admin
                 * cd C:\Users\Janek\Desktop\PLA4\FilesMonitor\FilesMonitor\bin\Debug
                 * installutil.exe FilesMonitor.exe
                 * services.msc
                 * installutil.exe /u FilesMonitor.exe
                 **/

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new FilesMonitoringService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
