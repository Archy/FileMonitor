﻿using log4net;

namespace FilesMonitor
{
    public static class LogExt
    {
        public static readonly log4net.Core.Level RenameLevel= new log4net.Core.Level(60000, "RENAME");

        public static void Rename(this ILog log, System.Type type, string from, string to)
        {
            log.Logger.Log(type, RenameLevel,
                            string.Format("Renamed: {0} to {1}", from, to), 
                            null);
        }
    }
}
