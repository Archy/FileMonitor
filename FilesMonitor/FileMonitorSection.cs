﻿using System;
using System.Collections;
using System.Text;
using System.Configuration;
using System.Xml;

namespace FilesMonitor
{
    public class FileMonitorSection : ConfigurationSection
    {
        public FileMonitorSection() { }

        public FileMonitorSection(FileMonitorSection fms)
        {
            Path = String.Copy(fms.Path);
            Recursive = fms.Recursive;
            Mask = String.Copy(fms.Mask);

            Created = fms.Created;
            Changed = fms.Changed;
            Deleted = fms.Deleted;
            Renamed = fms.Renamed;
            Disposed = fms.Disposed;
            Error = fms.Error;
        }


        [ConfigurationProperty("path", IsRequired = true)]
        //[RegexStringValidatorAttribute(@"^[a-zA-Z]:\\[\\\S|*\S]?.*$")]
        public String Path
        {
            get
            {
                return (String)this["path"];
            }
            set
            {
                this["path"] = value;
            }
        }

        [ConfigurationProperty("recursive", DefaultValue = "false", IsRequired = false)]
        public Boolean Recursive
        {
            get
            {
                return (Boolean)this["recursive"];
            }
            set
            {
                this["recursive"] = value;
            }
        }

        [ConfigurationProperty("mask", DefaultValue = "*.*", IsRequired = false)]
        public String Mask
        {
            get
            {
                return (String)this["mask"];
            }
            set
            {
                this["mask"] = value;
            }
        }


        #region EVENTS

        [ConfigurationProperty("changed", DefaultValue = "true", IsRequired = false)]
        public Boolean Changed
        {
            get
            {
                return (Boolean)this["changed"];
            }
            set
            {
                this["changed"] = value;
            }
        }

        [ConfigurationProperty("created", DefaultValue = "true", IsRequired = false)]
        public Boolean Created
        {
            get
            {
                return (Boolean)this["created"];
            }
            set
            {
                this["created"] = value;
            }
        }

        [ConfigurationProperty("deleted", DefaultValue = "true", IsRequired = false)]
        public Boolean Deleted
        {
            get
            {
                return (Boolean)this["deleted"];
            }
            set
            {
                this["deleted"] = value;
            }
        }

        [ConfigurationProperty("disposed", DefaultValue = "true", IsRequired = false)]
        public Boolean Disposed
        {
            get
            {
                return (Boolean)this["disposed"];
            }
            set
            {
                this["disposed"] = value;
            }
        }

        [ConfigurationProperty("error", DefaultValue = "true", IsRequired = false)]
        public Boolean Error
        {
            get
            {
                return (Boolean)this["error"];
            }
            set
            {
                this["error"] = value;
            }
        }

        [ConfigurationProperty("renamed", DefaultValue = "true", IsRequired = false)]
        public Boolean Renamed
        {
            get
            {
                return (Boolean)this["renamed"];
            }
            set
            {
                this["renamed"] = value;
            }
        }

        #endregion
    }
}
