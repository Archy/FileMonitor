﻿using FileMonitorWCF;
using System;
using System.Diagnostics;
using System.IO;
using System.ServiceModel;
using System.Threading;

namespace FilesMonitor
{
    class FileMonitor : IFileMonitorConfigService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FileMonitor));
        private static readonly object syncLock = new object();

        public bool IsRunning { get; set; } = true;
        public bool Recursive { get; set; } = true;

        private static FileSystemWatcher filesWatcher = new FileSystemWatcher();
        private static FileMonitorSection config;

        private static CounterCreationDataCollection col;
        private static CounterCreationData allCounterData;
        private static CounterCreationData txtCounterData;
        private static CounterCreationData docCounterData;

        private static volatile PerformanceCounter allCounter;
        private static volatile PerformanceCounter txtCounter;
        private static volatile PerformanceCounter docCounter;



        public void Run(FileMonitorSection config)
        {
            try
            {
                FileMonitor.config = new FileMonitorSection(config);
                Recursive = false; // config.Recursive;

                //log config
                log.Error(config.Path);
                log.Error(config.Recursive);
                log.Error(config.Mask);

                log.Error(config.Changed);
                log.Error(config.Created);
                log.Error(config.Deleted);
                log.Error(config.Disposed);
                log.Error(config.Error);
                log.Error(config.Renamed);

                createCounters();
                createWatcher(); //@"C:\Users\Janek\Desktop\monitor"

                //WCF Service
                using (ServiceHost host = new ServiceHost(
                        typeof(FileMonitor),
                        new Uri[]{
                        new Uri("net.pipe://localhost")
                        }
                    )
                )
                {
                    host.AddServiceEndpoint(typeof(IFileMonitorConfigService),
                      new NetNamedPipeBinding(),
                      "PipeFileMonitor");

                    host.Open();

                    while (IsRunning)
                    {
                        Thread.Sleep(10000);
                    }

                    host.Close();

                    allCounter.Close();
                    txtCounter.Close();
                    docCounter.Close();

                    PerformanceCounterCategory.Delete("FileMonitorPerfCounter");
                }
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
                throw e;
            }
        }

        public void onPause()
        {

            filesWatcher.EnableRaisingEvents = false;
        }

        public void onContinue()
        {

            filesWatcher.EnableRaisingEvents = true;
        }

        private void createWatcher()
        {
            filesWatcher = new FileSystemWatcher();
            filesWatcher.Path = config.Path;
            filesWatcher.Filter = config.Mask;
            filesWatcher.IncludeSubdirectories = config.Recursive;

            filesWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            if(config.Changed) filesWatcher.Changed += new FileSystemEventHandler(OnChanged);
            if (config.Created) filesWatcher.Created += new FileSystemEventHandler(OnCreate);
            if (config.Deleted) filesWatcher.Deleted += new FileSystemEventHandler(OnChanged);
            if (config.Renamed) filesWatcher.Renamed += new RenamedEventHandler(OnRenamed);
            if (config.Error) filesWatcher.Error += new ErrorEventHandler(OnError);
            if (config.Disposed) filesWatcher.Disposed += new EventHandler(OnDispose);

            filesWatcher.EnableRaisingEvents = true;
        }

        private void createCounters()
        {
            /**
             * lodctr /r
             **/
            if (!PerformanceCounterCategory.Exists("FileMonitorPerfCounter"))
            {
                col = new CounterCreationDataCollection();

                allCounterData = new CounterCreationData();
                allCounterData.CounterName = "AllFileChanges";
                allCounterData.CounterHelp = "File changes counter";
                allCounterData.CounterType = PerformanceCounterType.NumberOfItems32;

                txtCounterData = new CounterCreationData();
                txtCounterData.CounterName = "txtFileChanges";
                txtCounterData.CounterHelp = "txt file changes counter";
                txtCounterData.CounterType = PerformanceCounterType.NumberOfItems32;

                docCounterData = new CounterCreationData();
                docCounterData.CounterName = "docFileChanges";
                docCounterData.CounterHelp = "doc file changes counter";
                docCounterData.CounterType = PerformanceCounterType.NumberOfItems32;

                col.Add(allCounterData);
                col.Add(txtCounterData);
                col.Add(docCounterData);

            
                PerformanceCounterCategory category =
                    PerformanceCounterCategory.Create("FileMonitorPerfCounter",
                                                        "Custom file monitor perf counter", 
                                                        PerformanceCounterCategoryType.SingleInstance,
                                                        col);
                log.Info("FileMonitorPerfCounter created");
            }
            else
            {
                log.Info("FileMonitorPerfCounter already exists");
            }

            allCounter = new PerformanceCounter();
            allCounter.CategoryName = "FileMonitorPerfCounter";
            allCounter.CounterName = "AllFileChanges";
            allCounter.ReadOnly = false;

            txtCounter = new PerformanceCounter();
            txtCounter.CategoryName = "FileMonitorPerfCounter";
            txtCounter.CounterName = "txtFileChanges";
            txtCounter.ReadOnly = false;

            docCounter = new PerformanceCounter();
            docCounter.CategoryName = "FileMonitorPerfCounter";
            docCounter.CounterName = "docFileChanges";
            docCounter.ReadOnly = false;
        }

        private void OnCreate(object source, FileSystemEventArgs e)
        {
            log.Warn(e.ChangeType + ": " + e.FullPath);
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            log.Warn(e.ChangeType + ": " + e.FullPath);
            allCounter.Increment();
            if(e.FullPath.EndsWith(".txt"))
            {
                txtCounter.Increment();
            } 
            else if (e.FullPath.EndsWith(".doc") || e.FullPath.EndsWith(".docx"))
            {
                docCounter.Increment();
            }
        }

        private void OnRenamed(object source, RenamedEventArgs e)
        {
            log.Rename(typeof(FileMonitor), e.OldFullPath, e.FullPath);
        }

        private void OnError(object source, ErrorEventArgs e)
        {
            log.Error(e.GetException() + " " + e.GetException().Message);
        }

        private void OnDispose(object sender, EventArgs e)
        {
            log.Info("Dispose: " + e.GetType());
        }

        public bool ConfigureFileMonitor(FileMonitorConfig composite)
        {
            log.Info("Reconfigure: " + composite.ToString());

            config.Path = composite.Path;
            config.Recursive = composite.Recrusive;
            config.Mask = composite.Mask;
            //events:
            config.Changed = composite.Changed;
            config.Created = composite.Created;
            config.Deleted = composite.Deleted;
            config.Disposed = composite.Disposed;
            config.Error = composite.Error;
            config.Recursive = composite.Recrusive;

            createWatcher();
            return true;
        }
    }
}
