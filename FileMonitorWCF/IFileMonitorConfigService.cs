﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FileMonitorWCF
{
    [ServiceContract]
    public interface IFileMonitorConfigService
    {
        [OperationContract]
        bool ConfigureFileMonitor(FileMonitorConfig composite);
    }

    [DataContract]
    public class FileMonitorConfig
    {
        [DataMember]
        public string Path { get; set; } = @"C:\Users\Janek\Desktop\monitor";
        [DataMember]
        public bool Recrusive { get; set; } = true;
        [DataMember]
        public string Mask { get; set; } = "*.*";

        [DataMember]
        public bool Changed { get; set; } = true;
        [DataMember]
        public bool Created { get; set; } = true;
        [DataMember]
        public bool Deleted { get; set; } = true;
        [DataMember]
        public bool Disposed { get; set; } = true;
        [DataMember]
        public bool Error { get; set; } = true;
        [DataMember]
        public bool Renamed { get; set; } = true;

        public override string ToString() {
            StringBuilder str = new StringBuilder();

            str.AppendLine("Path: " + Path);
            str.AppendLine("Recrusive: " + Recrusive);
            str.AppendLine("Mask: " + Mask);

            str.AppendLine("Changed: " + Changed);
            str.AppendLine("Created: " + Created);
            str.AppendLine("Deleted: " + Deleted);

            str.AppendLine("Disposed: " + Disposed);
            str.AppendLine("Error: " + Error);
            str.AppendLine("Renamed: " + Renamed);

            return str.ToString();
        }
    }
}
